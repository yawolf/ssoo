#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <dlfcn.h>
#include "plugin_info.h"
#include <signal.h>
#include <unistd.h>

void leer_mandatos(FILE*, int con_prompt);

void mandato_load  (char* arg);
void mandato_start ();
void mandato_stop  ();
void mandato_sleep (char* arg);
void mandato_echo  (char* arg);
void mandato_finish();

plugin_info_t *(*funcionPlugin)() = NULL;
int opcion = 0, iniciados, enProceso;
pid_t pid;

#define VERDADERO 1
#define FALSO     0

int main(int argc, char* argv[]) {
	FILE* fich;

	if (!strcmp(argv[1],"-p")) opcion = 0;
	else opcion = 1;

	if(argc<2 || !strcmp(argv[1],"--help")) {
		fprintf(stderr,
				"%s --help: Muestra la ayuda\n"
				"%s [-t|-p] [fichero_ordenes]\n",argv[0],argv[0]);
		return 0;
	}


	if(argc==3) {
		fich=fopen(argv[2],"r");
		if(fich==NULL) {
			fprintf(stderr,
					"%s --help: Muestra la ayuda\n"
					"%s [-t|-p] [ficher_ordenes]\n",argv[0],argv[0]);
			return 0;
		}
		leer_mandatos(fich,FALSO); 
	}
	else
		leer_mandatos(stdin,VERDADERO); 

	return 0;
}

void leer_mandatos(FILE* fich, int prompt) {
	int salir=0;
	char linea[1024];
	char mandato[1024];
	char argumento[1024];

	while(!salir)  {
		if(prompt)
			fprintf(stdout,"cargador> ");

		if(fgets(linea,1024,fich)) {
			bzero(argumento,sizeof(argumento));
			sscanf(linea,"%s %[^\n]",mandato,argumento);

			if     (!strcmp(mandato,"load" ))
				mandato_load(argumento);
			else if(!strcmp(mandato,"start" ))
				mandato_start();
			else if(!strcmp(mandato,"stop"  ))
				mandato_stop(argumento);
			else if(!strcmp(mandato,"sleep" ))
				mandato_sleep(argumento);
			else if(!strcmp(mandato,"finish"))
				mandato_finish(argumento);
			else if(!strcmp(mandato,"echo"  ))
				mandato_echo(argumento);
			else if(!strcmp(mandato,"quit"  ))
				salir=1;
			else
				fprintf(stderr,"Mandato: '%s' no valido\n",mandato);
		}
	}
}

void mandato_load  (char* arg) {

	void *cargado;
	char * error;
	cargado=dlopen(arg,RTLD_NOW);

	if(!cargado){
		fprintf(stderr,"%s: Error(1), No se puede cargar\n",arg);
		exit(1);
	}else{
		funcionPlugin = (plugin_info_t *(*)()) dlsym (cargado,"plugin_info");
		if((error = dlerror()) != NULL){
			fprintf(stderr,"%s: Error(2), No se encuentra simbolo\n",arg);
			exit(2);
		}
	}
	fprintf(stderr,"Modulo %s cargado correctamente\n",arg);

}

void loopin () {
	plugin_info_t *puntero = (*funcionPlugin)();
	while (1) {
		(puntero->plugin_loop)();
	}
}

void signalAcaba (){
	plugin_info_t *puntero = (*funcionPlugin)();
	exit(puntero->plugin_finish());
}
void signalRearranca (){
	plugin_info_t *puntero = (*funcionPlugin)();
	(puntero->plugin_resume());
}
void signalPara (){
	plugin_info_t *puntero = (*funcionPlugin)();
	(puntero->plugin_stop());
}

void mandato_start () {
	if(opcion == 0){
		//int status;
		//pid_t pid;
		struct sigaction actAcaba, actRearranca, actPara;
		switch(pid=fork()){
			case -1:
				fprintf(stderr,"Fork: Error(5), Error al crear instancia\n");	
				exit(5);
			case 0:
				actAcaba.sa_handler=&signalAcaba;
				actAcaba.sa_flags=0;
				actRearranca.sa_handler=signalRearranca;
				actRearranca.sa_flags=0;
				actPara.sa_handler=&signalPara;
				actPara.sa_flags=0;
				sigaction(SIGTERM,&actAcaba,NULL);
				sigaction(SIGUSR1,&actPara,NULL);
				sigaction(SIGCONT,&actRearranca,NULL);
				loopin();

			default:
				iniciados++;
				enProceso++;
		}

		fprintf(stderr,"Modulo iniciado: %d\n",iniciados);
		fprintf(stderr,"Numero de modulos iniciados: %d\n",enProceso);
	}else if(opcion == 1){
		fprintf(stderr,"Modulo iniciado: %d\n",iniciados);
		fprintf(stderr,"Numero de modulos iniciados: %d\n",enProceso);
	}else{
		fprintf(stderr,"Thread: Error(3), Parametro de mandato no valido\n");
		exit(3);
	}
}

void mandato_stop  (char* arg) {	
	if(opcion == 0){
		kill(pid,SIGUSR1);
		enProceso --;
		fprintf(stderr,"Modulo detenido: %d\n",0);
	}else if(opcion == 1){

		fprintf(stderr,"Modulo detenido: %d\n",0);
	}

}


void mandato_sleep (char* arg) {	
	struct timespec tiempo;
	tiempo.tv_sec=atoi(arg);
	tiempo.tv_nsec=0;

	nanosleep(&tiempo,&tiempo);	
}

void mandato_echo  (char* arg) { 	fprintf(stderr,"%s\n",arg); }



void mandato_finish(char* arg) {
	int status;
	if(opcion == 0){
		kill(pid,SIGTERM);
		waitpid(pid,&status,0);
		iniciados --;
		enProceso --;
		fprintf(stderr,"Modulo finalizado: %d (%d)\n",0,WEXITSTATUS(status));
	}else if(opcion == 1){

	}else{
		fprintf(stderr,"%s: Error(3), Parametro de mandato no valido\n");
		exit(3);
	}
}

