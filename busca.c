#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>

#define TAM 2048
#define prog "./busca"

void htout(char *name, char *hto, char **env, int size){
	int cnt, s, ret;
	pid_t pid, t_pid;
	char *env2[size];

	if (!strcmp(hto,"-print")){
		fprintf(stdout,"%s\n",name);

	} else if (!strcmp(hto,"-exec")){

		for (cnt=0; cnt <= size; cnt++){
			env2[cnt] = env[cnt];
			if (cnt == size-1) env2[cnt] = name;
			else if (cnt == size) env2[size] = NULL;
		}

		name = env[0];

		pid = fork();

		switch (pid){
			case -1:
				perror("fork");
				exit(2);
			case 0:
				ret = execvp(env2[0],env2);
				if (ret < 0){
					fprintf(stderr,"%s: Error(%d), No se puede ejecutar el mandato\n",prog,4);
					exit(4);
				}

			default:

				while (wait(&s) != pid);
		}
	}
}

void by_content(char *path, char *mode, char *arg, char *hto, char **env, int size){
	struct stat dFile;
	struct dirent *rdir;
	FILE *pfile;
	char buff[100], line[TAM];
	int ret, file;
	void *mfile;
	DIR *dir;

	dir = opendir(path);

	if (dir == NULL){
		fprintf(stderr,"%s: Error(%d), No se puede leer el directorio.\n",prog,2);
		exit(2);
	}

	while ((rdir = readdir(dir)) != NULL) {

		snprintf(buff,100,"%s/%s",path,rdir->d_name);

		if (size > 0)
			env[size]=buff;

		ret = stat(buff, &dFile);
		if (ret < 0){
			fprintf(stderr,"%s: Error(%d), No se puede leer el archivo\n",prog,3);
			exit(3);
		}

		if (S_ISREG(dFile.st_mode) != 0 && rdir->d_name[0] != '.') {
			file = open(buff, O_RDONLY);
			if (file > 0){
				if (!strcmp(mode,"-c")){
					pfile = fdopen(file, "r"); /*<- Esto se puede borrar si se usa el fd y read*/
					while(fgets(line,TAM,pfile) != NULL){
						if (strstr(line,arg) != NULL){
							fgets(line,0,pfile);
							htout(buff,hto,env,size);
							close(file);
							break;
						}
					}

				} else if (!strcmp(mode,"-C")) {
					mfile = mmap(NULL,dFile.st_size,PROT_READ,MAP_PRIVATE,file,0);
					if (mfile == MAP_FAILED){
						perror("mmap");
						exit(2);
					}
					if (strstr(mfile,arg) != NULL){
						htout(buff,hto,env,size);
						munmap(mfile, dFile.st_size);
					}
				}
				close(file);
			} else{
				fprintf(stderr,"%s: Error(%d), No se puede leer el archivo\n",prog,3);
			}
		}
	}
}

void by_perm(char *path, char *arg, char *hto, char **env, int size){
	struct stat dFile;
	struct dirent *rdir;
	char p, buff[100];
	int ret;
	DIR *dir;

	dir = opendir(path);

	if (dir == NULL){
		fprintf(stderr,"%s: Error(%d), No se puede leer el directorio.\n",prog,2);
		exit(2);
	}

	while ((rdir = readdir(dir)) != NULL ){

		snprintf(buff,100,"%s/%s",path,rdir->d_name);

		if (size > 0)
			env[size-1]=buff;

		ret = stat(buff, &dFile);
		if (ret < 0){
			perror("stat");
			exit(2);
		}

		p = rdir->d_name[0];

		if (p != '.'){

			if (strcmp(arg,"r") && strcmp(arg,"w") && strcmp(arg,"x")){
				fprintf(stderr,"%s: Error(%d), Opcion no valida\n",prog,1);
				exit(1);
			}

			if(dFile.st_uid == geteuid()){
				if ((dFile.st_mode & S_IRUSR) != 0 && !strcmp(arg,"r")){
					htout(buff,hto,env,size);
				}else if ((dFile.st_mode & S_IWUSR) != 0 && !strcmp(arg,"w")){
					htout(buff,hto,env,size);
				}else if ((dFile.st_mode & S_IXUSR) != 0 && !strcmp(arg,"x")){
					htout(buff,hto,env,size);
				}

			}else if(dFile.st_gid == getegid()){
				if ((dFile.st_mode & S_IRGRP) != 0 && !strcmp(arg,"r")){
					htout(buff,hto,env,size);
				}else if ((dFile.st_mode & S_IWGRP) != 0 && !strcmp(arg,"w")){
					htout(buff,hto,env,size);
				}else if ((dFile.st_mode & S_IXGRP) != 0 && !strcmp(arg,"x")){
					htout(buff,hto,env,size);
				}

			}else{
				if ((dFile.st_mode & S_IROTH) != 0 && !strcmp(arg,"r")){
					htout(buff,hto,env,size);
				}else if ((dFile.st_mode & S_IWOTH) != 0 && !strcmp(arg,"w")){
					htout(buff,hto,env,size);
				}else if ((dFile.st_mode & S_IXOTH) != 0 && !strcmp(arg,"x")){
					htout(buff,hto,env,size);
				}
			}
		}
	}
	closedir(dir);
}

void by_name (char *path, char *arg3, char *hto, char **env, int size){
	struct stat dFile;
	struct dirent *rdir;
	char p, buff[100];
	int ret;
	DIR *dir;

	dir = opendir(path);

	if (dir == NULL){
		fprintf(stderr,"%s: Error(%d), No se puede leer el directorio.\n",prog,2);
		exit(2);
	}

	while ((rdir = readdir(dir)) != NULL ){

		snprintf(buff,100,"%s/%s",path,rdir->d_name);

		if (size > 0)
			env[size]=buff;

		ret = stat(buff, &dFile);
		if (ret < 0){
			perror("stat");
			exit(2);
		}

		stat(buff, &dFile);

		p = rdir->d_name[0];
		if (p != '.'){

			if (strstr(rdir->d_name, arg3) != NULL){
				htout(buff,hto,env,size);
			}
		}
	}
	closedir(dir);
}

int by_type (char *path, char *arg3, char *hto, char **env, int size){
	struct dirent *rdir;
	struct stat dFile;
	char p, buff[100];
	int ret;
	DIR *dir;

	dir = opendir(path);

	if (dir == NULL){
		fprintf(stderr,"%s: Error(%d), No se puede leer el directorio.\n",prog,2);
		exit(2);
	}

	while ((rdir = readdir(dir)) != NULL ){

		snprintf(buff,100,"%s/%s",path,rdir->d_name);

		if (size > 0)
			env[size]=buff;

		ret = stat(buff, &dFile);
		if (ret < 0){
			perror("stat");
			exit(2);
		}

		p = rdir->d_name[0];

		if (p != '.'){
			if (!strcmp(arg3,"f")){
				if (S_ISREG(dFile.st_mode) != 0){
					htout(buff,hto,env,size);
				}
			}else if (!strcmp(arg3,"d")){
				if (S_ISDIR(dFile.st_mode) != 0){
					htout(buff,hto,env,size);
				}
			}else{
				fprintf(stderr,"%s: Error(%d), Opcion no valida\n",prog,1);
				exit(1);
			}
		}
	}
	closedir(dir);
}

int main(int argc, char* argv[]){
	int i = 0, j = 0;
	char *env[argc-5];

	if(argc<2 || !strcmp(argv[1],"-h")){
		fprintf(stdout,
				"busca -h: Muestra la ayuda\n"
				"busca dir [-n|-t|-p|-c|-C] argumento [-R] "
				"[-print|-pipe|-exec] [mandato argumentos..]\n");
		return 0;
	}

	switch (argc){
		case 1:
		case 2:
		case 3:
		case 4:
			fprintf(stderr,"%s: Error(%d), Opcion no valida\n",prog,2);
			exit(2);
			break;

		case 5:
			if (!strcmp(argv[4],"-exec") || !strcmp(argv[4],"-pipe")){
				fprintf(stderr,"%s: Error(%d), Opcion no valida\n",prog,2);
				exit(2);
			}

			if (!strcmp(argv[2],"-t")){
				by_type(argv[1],argv[3],argv[4],NULL,0);
			} else if (!strcmp(argv[2],"-n")){
				by_name(argv[1],argv[3],argv[4],NULL,0);
			} else if (!strcmp(argv[2],"-p")){
				by_perm(argv[1],argv[3],argv[4],NULL,0);
			} else if (!strcmp(argv[2],"-c") || !strcmp(argv[2],"-C")){
				by_content(argv[1],argv[2],argv[3],argv[4],NULL,0);
			} else{
				fprintf(stderr,"%s: Error(%d), Opcion no valida\n",prog,2);
				exit(2);
			}
			break;
		default:
			for (i=5; i<=argc; i++){
				env[j]=argv[i];
				j++;
			}
			if (!strcmp(argv[4],"-exec") || !strcmp(argv[4],"-pipe")){
				if (!strcmp(argv[2],"-t")){
					by_type(argv[1],argv[3],argv[4],env,j);
				} else if (!strcmp(argv[2],"-n")){
					by_name(argv[1],argv[3],argv[4],env,j);
				} else if (!strcmp(argv[2],"-p")){
					by_perm(argv[1],argv[3],argv[4],env,j);
				} else if (!strcmp(argv[2],"-c") || !strcmp(argv[2],"-C")){
					by_content(argv[1],argv[2],argv[3],argv[4],env,j);
				} else{
					fprintf(stderr,"%s: Error(%d), Opcion no valida\n",prog,2);
					exit(2);
				}
			} else{
				fprintf(stderr,"%s: Error(%d), Opcion no valida\n",prog,2);
				exit(2);
			}
	}
	return 0;
}
